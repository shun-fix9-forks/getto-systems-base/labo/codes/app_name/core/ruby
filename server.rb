require "grpc"
require "app_name/api/core_server"
require "app_name/api/repo"

GRPC::RpcServer.new.tap{|server|
  GettoCodes::AppName::Api::CoreServer.api(
    repo: GettoCodes::AppName::Api::Repo.new(ENV["REPO_URL"]),
  )

  server.add_http2_port "0.0.0.0:3010", :this_port_is_insecure
  server.handle GettoCodes::AppName::Api::CoreServer
  server.run_till_terminated_or_interrupted [ "SIGTERM" ]
}
