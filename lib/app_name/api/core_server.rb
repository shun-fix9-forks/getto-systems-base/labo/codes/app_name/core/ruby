require "app_name/pb/core_services_pb"

require "app_name/api/version"

module GettoCodes
  module AppName
    module Api
      class CoreServer < Pb::Core::Service
        class << self
          def api(api)
            self.instance_exec do
              define_method :api do
                api
              end
            end
          end
        end

        def version(req, _call)
          Pb::Core::Version::Response.new(
            core: VERSION,
            repo: api[:repo].version[:number],
          )
        end
      end
    end
  end
end
