require "grpc"
require "app_name/pb/repo_services_pb"

module GettoCodes
  module AppName
    module Api
      class Repo
        def initialize(host)
          @stub = Pb::Repo::Stub.new host, :this_channel_is_insecure
        end

        attr_reader :stub

        def version
          res = stub.version(Pb::Repo::Version::Request.new)
          {
            number: res.number,
          }
        end
      end
    end
  end
end
